package com.example.a6150110019;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {


    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }
    public void onClickNext(View view){
        Button btn_next = (Button)findViewById(R.id.but1);
        Intent intent = new Intent(MainActivity.this,Page2.class);
        startActivity(intent);
    }
    public void onClickHome(View view){
        Button btn_next = (Button)findViewById(R.id.but2);
        finish();
    }
    public void onClickMe(View view){
        Button btn_next = (Button)findViewById(R.id.but2);
        Intent intent = new Intent(MainActivity.this,Page3.class);
        startActivity(intent);
    }

}