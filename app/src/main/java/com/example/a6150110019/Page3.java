package com.example.a6150110019;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Page3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page3);
    }
    public void onClickHome(View view){
        Button btn_next = (Button)findViewById(R.id.but2);
        Intent intent = new Intent(Page3.this,MainActivity.class);
        startActivity(intent);
    }
}