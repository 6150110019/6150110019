package com.example.a6150110019;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Page2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page2);

    }
    public void onClick1(View view) {
        String myUriString = "https://www.youtube.com/results?search_query=%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B9%8C%E0%B8%95%E0%B8%B9%E0%B8%99";
        Intent button1 = new Intent(Intent.ACTION_VIEW, Uri.parse(myUriString));
        startActivity(button1);
    }

    public void onClick2(View view) {
        String myUriString = "https://www.youtube.com/results?search_query=%E0%B8%82%E0%B9%88%E0%B8%B2%E0%B8%A7";
        Intent button2 = new Intent(Intent.ACTION_VIEW, Uri.parse(myUriString));
        startActivity(button2);
    }
    public void onClick3(View view) {
        String myUriString = "https://www.youtube.com/results?search_query=%E0%B8%81%E0%B8%B5%E0%B8%AC%E0%B8%B2";
        Intent button3 = new Intent(Intent.ACTION_VIEW, Uri.parse(myUriString));
        startActivity(button3);
    }
    public void onClick4(View view) {
        String myUriString = "https://www.youtube.com/results?search_query=%E0%B9%80%E0%B8%9E%E0%B8%A5%E0%B8%87";
        Intent button4 = new Intent(Intent.ACTION_VIEW, Uri.parse(myUriString));
        startActivity(button4);
    }
    public void onClick5(View view) {
        String myUriString = "https://www.youtube.com/results?search_query=%E0%B8%95%E0%B8%A5%E0%B8%81";
        Intent button5 = new Intent(Intent.ACTION_VIEW, Uri.parse(myUriString));
        startActivity(button5);
    }
    public void onClick6(View view) {
        String myUriString = "https://www.youtube.com/results?search_query=%E0%B8%82%E0%B9%88%E0%B8%B2%E0%B8%A7%E0%B8%95%E0%B9%88%E0%B8%B2%E0%B8%87%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B9%80%E0%B8%97%E0%B8%A8";
        Intent button6 = new Intent(Intent.ACTION_VIEW, Uri.parse(myUriString));
        startActivity(button6);
    }
    public void onClick7(View view) {
        String myUriString = "https://www.youtube.com/results?search_query=%E0%B8%A5%E0%B8%B0%E0%B8%84%E0%B8%A3";
        Intent button7 = new Intent(Intent.ACTION_VIEW, Uri.parse(myUriString));
        startActivity(button7);
    }
    public void onClick8(View view) {
        String myUriString = "https://www.youtube.com/results?search_query=%E0%B8%81%E0%B8%A5%E0%B9%88%E0%B8%AD%E0%B8%A1%E0%B8%99%E0%B8%AD%E0%B8%99";
        Intent button8 = new Intent(Intent.ACTION_VIEW, Uri.parse(myUriString));
        startActivity(button8);
    }
    public void onClickHome(View view){
        Button btn_next = (Button)findViewById(R.id.but1);
        Intent intent = new Intent(Page2.this,MainActivity.class);
        startActivity(intent);
    }

}
